# Debian LFTP

A debian slim container that has [lftp](https://lftp.yar.ru/) preinstalled. 

## How to use

Run DebianLFTP as part of your deployment pipeline to transfer files to production. You must have an .lftprc file located in the container's home path, `~/`. 
You can learn more about .lftprc settings on the [lftp man page](https://lftp.yar.ru/lftp-man.html), under 'Settings'.

## GitLab CI Example

The example below shows how to set up your GitLab repository for continuous delivery. It assumes the following files exist:

- .gitlab-ci.yml
- scripts/.lftprc
- scripts/deploy.sh
- scripts/deploy_pages.sh

**.gitlab-ci.yml**

```yml
stages:
  - build
  - deploy

build:production:
  artifacts:
    paths:
    # learn more about artifacts here: 
    # https://docs.gitlab.com/ee/user/project/pipelines/job_artifacts.html
      - prod

deploy:production:
  stage: deploy
  image: registry.gitlab.com/theetrain/containers/debianlftp:latest
  environment:
    name: production
    url: [production URL goes here]
  variables:
    BUILD_PATH: /builds/repository_path/prod # example: /builds/theetrain/containers/prod
    REMOTE_PATH: $PRODUCTION_PATH # create an environment variable in your project with the same name
  before_script:
   # wherever you store your .lftprc file in your repository, it must be copied to the DebiantLFTP container's
   # home path. See .lftprc example below.
    - cp scripts/.lftprc ~/
  script:
    - ./scripts/deploy.sh
  when: manual
  only:
    - master
  dependencies:
    - build:production
```

**.lftprc**

```sh
set ftp:ssl-allow true
set ssl:verify-certificate no
```

**deploy.sh**

```sh
#!/bin/bash

# Deploys pages to production after building

# Verify assets and variables
missing=""
[ -z "$BUILD_PATH" ]   && missing="$missing BUILD_PATH"
[ -z "$REMOTE_PATH" ]  && missing="$missing REMOTE_PATH"
[ -z "$FTP_HOST" ]     && missing="$missing FTP_HOST"
[ -z "$USER" ]    && missing="$missing USER"
[ -z "$PASS" ]    && missing="$missing PASS"

if [ ! -z "$missing" ]
then
  >&2 echo "$missing var is missing"
  exit 1
fi

./deploy_pages.sh
```

**deploy_pages.sh**

```sh
#!/bin/bash

echo "Prepping $BUILD_PATH"

cd ${BUILD_PATH}
echo "Current directory: $PWD"

if [[ "$PWD" != *"$BUILD_PATH"* ]]
then
  >&2 echo "current working directory invalid"
  echo "PWD is $PWD; and target directory is $BUILD_PATH"
  exit 1
fi

echo "Deploying $BUILD_PATH"

echo "machine $FTP_HOST login $CCEM_USER password $CCEM_PASS" > ~/.netrc

# This will aggressively overwrite remote files with those in the build artifact
# See the 'mirror' command in the lftp man page
# https://lftp.yar.ru/lftp-man.html
lftp -e "cd $REMOTE_PATH; mirror -R; bye" $FTP_HOST
```